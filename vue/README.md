# Vue CI/CD in GitLab 

## Features

But wait!... first check this:

1. Simplicity: This pipeline is created as a simulation, the plan here is to make it simple and would need to be adjusted to dev-team way of work.
2. Costs: Multiple people pushing changes may incur in computational costs.
3. Costs-again: If every what we commit goes to the artifactory, storage costs is also something to consider.

### ci/cd template based pipeline

This is the gitlab-ci-templates repository, you are free to use these pipelines and more than happy to get your MRs.

### Artifact Repository

Build jobs uses ```REPOSITORY``` variable to define two different set of artifacts:

**Release-Candidates** 

Or ```releases```. The ```build release``` stage will run only if a tag is created. All tags must be protected.
The idea behind this is that a release should(must) be immutable.

A new release will be created in GitLab once you create a new tag.

**dev-versions**

Or ```snapshots```. The ```build snapshot``` job will build the artifact, no matter if you commit any branch or merge by MR.

### Tests

The project will run unit-tests, report is available in GitLab. This pipeline is not restricting any deployment from unit tests, this is also a decision to make in the team.

### Deployment

* You would need to define the S3 Bucket where the src should be deployed.

```yaml
include:
  - project: 'odvillarraga/gitlab-ci-templates'
    file: '/vue/vue.gitlab-ci.yml'

...                     # Variables and other definitions you may put.
...                     #

deploy snapshot:
  variables:
    S3_BUCKET_NAME: ''  # Here the bucket for deploying in dev.

deploy release:
  variables:
    S3_BUCKET_NAME: # Here the bucket for deploying in prod.

```

* Continuous delivery is activated for every single commit in dev environment.
* It requires a manual execution to deploy in prod.




